
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>
    <link rel="stylesheet" href="color_form.css">
    <script type="text/javascript">
        $(function () {
            $('#txtDate').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>
</head>
<body>

   
<?php
   $error = array();
   $data = array();
   if (!empty($_POST['submitt'])) {
	   $data['name'] = isset($_POST['name']) ? $_POST['name'] : '';
	   $data['gender'] = isset($_POST['gender']) ? $_POST['gender'] : '';
	   $data['group'] = isset($_POST['group']) ? $_POST['group'] : '';
	   $data['date'] = isset($_POST['date']) ? $_POST['date'] : '';
	   if (empty($data['name'])) {
		   $error['name'] = '<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
		   Hãy nhập tên.</i> ';
	   }
	   
	   if (empty($data['gender'])) {
		   $error['gender'] = '<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
		   Hãy chọn giới tính.</i> ';
	   }

	   if (empty($data['group'])) {
		   $error['group'] = '<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
		   Hãy chọn phân khoa.</i> ';
	   }
	   if(empty($data['date'])) {
		   $error['date'] = '<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
		   Hãy nhập ngày sinh.</i> ';
	   } else {
		   $datee = explode("/",$data['date']);
		   if(!checkdate($datee[1] ,$datee[0] ,$datee[2]))
		   {
			   $error['date'] = '<i style="color:red;font: size 15px; font-family: "Times New Roman", Times, serif ;">
		   Vui lòng nhập đúng định dạng ngày sinh.</i> ';
		   }

	   }
	   
	   if (!$error) {
		   echo 'Đã đăng ký thành công';
	   }
   }
   ?>


	<div class="form_center">	
	    <table class="table_form">
	     	<form method="post" action="regist.php" enctype='multipart/form-data'>
			<tr>
				<td class="form">
			<?php echo isset($error['name']) ? $error['name'] : ''; ?> <br />
			<?php echo isset($error['gender']) ? $error['gender'] : ''; ?> <br />
			<?php echo isset($error['group']) ? $error['group'] : ''; ?> <br />
			<?php echo isset($error['date']) ? $error['date'] : ''; ?> <br />
				</td>
			</tr>
			
		    	<tr>
				<td><div class="form_td">Họ và tên<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div> </td>
					<td ><input type="text" class="input_form" name="name" size="30"></td>
				</tr>
				
				<tr>
				<td><div class="form_td">Giới tính<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
				<td><?php
                $Gender = array("0" => "Nam", "1" => "Nữ");
                for ($i = 0; $i <= count($Gender) - 1; $i++) { ?>
                <input type="radio" name="gender" checked="<?php echo "checked"; ?>"
                    value=" <?php echo $Gender[$i]; ?>"> <?php echo $Gender[$i]; ?>
                <!-- <input type="radio" name="<?php echo $i; ?>"> <?php echo $Gender[$i]; ?> -->
                <?php } ?>

					</td>
					
				</tr>
				</tr>
				<td><div class="form_td">Phân Khoa<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
					<td>
					<select class = "selectbox_color"  name = "group" >
                    <option value=""></option>
                    <?php $Depart = array( "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                    foreach ($Depart as $key => $value) { ?>
                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                    <?php } ?>
                </select>	</td>
				</tr>
				
				<tr>
				<td><div class="form_td">Ngày sinh<sup class="sup"><?php 
				echo '<i style="color:red;font: size 15px;; font-family: "Times New Roman", Times, serif ;">
					*</i> ';
				?> </sup></div>
                    <td>
                    <input name="date" class="date form-control field__input" type="text" style="padding: 22px 12px;">
                </div>
					</td>
				</tr>
				<tr>
				<td><div class="form_td">Địa chỉ<sup class="sup"></sup></div>
					<td ><input type="text" class="input_form" name="address" size="30"></td>
				</tr>
				<tr>
					<td colspan="2" align="center">

						<input class="form_submit" type="submit" method="post" name="submitt" value="Đăng ký">
					</td>
				</tr>
			</form>

		</table>
		<script type="text/javascript">  
    $('.date').datepicker({  
       format: 'dd/mm/yyyy'  
     });  
</script>
</body>
</html>